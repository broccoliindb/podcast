import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { PodcastsService } from './podcasts.service';
import { Podcast } from './entities/podcast.entity';
import { CreatePodcastDto } from './dto/create-podcast.dto';
import { UpdatePodcastDto } from './dto/update-podcast.dto';
@Controller('podcasts')
export class PodcastsController {
  constructor(private readonly podcastsService: PodcastsService) {}

  @Get()
  getAllPodcasts(): Podcast[] {
    return this.podcastsService.getAllPodcasts();
  }

  @Get('/:id')
  getPodcast(@Param('id') podcastId: number): Podcast {
    return this.podcastsService.getPodcast(podcastId);
  }

  @Post()
  createPodcast(@Body() podcastData: CreatePodcastDto) {
    return this.podcastsService.createPodcast(podcastData);
  }

  @Delete('/:id')
  deletePodcast(@Param('id') podcastId: number) {
    return this.podcastsService.deletePodcast(podcastId);
  }

  @Patch('/:id')
  updatePodcast(
    @Param('id') podcastId: number,
    @Body() updatePodcast: UpdatePodcastDto,
  ) {
    return this.podcastsService.updatePodcast(podcastId, updatePodcast);
  }
}
