import { Injectable, NotFoundException } from '@nestjs/common';
import { Podcast } from './entities/podcast.entity';
import { CreatePodcastDto } from './dto/create-podcast.dto';
import { UpdatePodcastDto } from './dto/update-podcast.dto';
@Injectable()
export class PodcastsService {
  private podcasts: Podcast[] = [];

  getAllPodcasts(): Podcast[] {
    return this.podcasts;
  }

  getPodcast(id: number): Podcast {
    const podcast = this.podcasts.find((podcast) => podcast.id === id);
    if (!podcast) {
      throw new NotFoundException(`Podcast with Id ${id} is not found`);
    }
    return podcast;
  }

  createPodcast(podCastData: CreatePodcastDto): Podcast {
    const podcast = {
      id: Date.now(),
      ...podCastData,
    };
    this.podcasts.push(podcast);
    return podcast;
  }

  deletePodcast(id: number) {
    this.getPodcast(id);
    this.podcasts = this.podcasts.filter((item) => item.id !== id);
  }

  updatePodcast(id: number, updateData: UpdatePodcastDto): Podcast {
    const podcast = this.getPodcast(id);
    this.deletePodcast(id);
    const updatedPodcast = { ...podcast, ...updateData };
    this.podcasts.push(updatedPodcast);
    return updatedPodcast;
  }
}
