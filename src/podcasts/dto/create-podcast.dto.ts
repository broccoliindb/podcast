import { Type } from 'class-transformer';
import { IsArray, IsNumber, IsString, ValidateNested } from 'class-validator';
import { Episode } from '../../episodes/entities/episode.entity';

export class CreatePodcastDto {
  @IsString()
  readonly title: string;

  @IsString()
  readonly category: string;

  @IsNumber()
  readonly rating: number;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => Episode)
  readonly episodes: Episode[];
}
