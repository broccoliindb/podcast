import { Module } from '@nestjs/common';
import { EpisodesController } from './episodes.controller';
import { EpisodesService } from './episodes.service';
import { PodcastsService } from 'src/podcasts/podcasts.service';
import { PodcastsController } from 'src/podcasts/podcasts.controller';

@Module({
  controllers: [EpisodesController, PodcastsController],
  providers: [EpisodesService, PodcastsService],
})
export class EpisodesModule {}
