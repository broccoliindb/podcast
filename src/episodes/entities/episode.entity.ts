import { IsNumber, IsString } from 'class-validator';

export class Episode {
  @IsNumber()
  id: number;

  @IsString()
  title: string;
}
