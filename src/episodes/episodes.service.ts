import { Injectable } from '@nestjs/common';
import { Episode } from './entities/episode.entity';
import { PodcastsService } from '../podcasts/podcasts.service';
import { CreateEpisodeDto } from './dto/create-episode.dto';
import { UpdateEpisodeDto } from './dto/update-episode.dto';

@Injectable()
export class EpisodesService {
  constructor(private readonly podcastsService: PodcastsService) {}

  getAllEpisodes(podcastId: number): Episode[] {
    const podcast = this.podcastsService.getPodcast(podcastId);
    return podcast.episodes;
  }

  createEpisode(podcastId: number, EpisodeData: CreateEpisodeDto): Episode {
    const podcast = this.podcastsService.getPodcast(podcastId);
    const Episode = {
      id: Date.now(),
      ...EpisodeData,
    };
    podcast.episodes.push(Episode);
    return Episode;
  }

  deleteEpisode(podcastId: number, id: number) {
    const podcast = this.podcastsService.getPodcast(podcastId);
    const episodes = podcast.episodes.filter((item) => item.id !== id);
    this.podcastsService.updatePodcast(podcastId, { episodes });
  }

  updateEpisode(
    podcastId: number,
    id: number,
    updateData: UpdateEpisodeDto,
  ): Episode {
    const podcast = this.podcastsService.getPodcast(podcastId);
    const deletedEpisodes = podcast.episodes.filter((item) => item.id !== id);
    const updatedEpisode = { id, ...updateData } as Episode;
    const episodes = [...deletedEpisodes, updatedEpisode];
    this.podcastsService.updatePodcast(podcastId, {
      episodes,
    });
    return updatedEpisode;
  }
}
