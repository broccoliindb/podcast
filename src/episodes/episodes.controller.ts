import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Patch,
} from '@nestjs/common';
import { Episode } from './entities/episode.entity';
import { EpisodesService } from './episodes.service';
import { CreateEpisodeDto } from './dto/create-episode.dto';
import { UpdateEpisodeDto } from './dto/update-episode.dto';
@Controller('podcasts/:id')
export class EpisodesController {
  constructor(private readonly episodesService: EpisodesService) {}

  @Get('/episodes')
  getAllEpisodes(@Param('id') podcastId: number): Episode[] {
    return this.episodesService.getAllEpisodes(podcastId);
  }

  @Post('/episodes')
  createEpisode(
    @Param('id') podcastId: number,
    @Body() episodeData: CreateEpisodeDto,
  ) {
    return this.episodesService.createEpisode(podcastId, episodeData);
  }

  @Delete('/episodes/:episodeId')
  deletePodcast(
    @Param('id') podcastId: number,
    @Param('episodeId') id: number,
  ) {
    return this.episodesService.deleteEpisode(podcastId, id);
  }

  @Patch('/episodes/:episodeId')
  updatePodcast(
    @Param('id') podcastId: number,
    @Param('episodeId') id: number,
    @Body() updatePodcast: UpdateEpisodeDto,
  ) {
    return this.episodesService.updateEpisode(podcastId, id, updatePodcast);
  }
}
